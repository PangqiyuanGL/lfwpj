function Y = Scatter_inclass(X,K,mu,V)
[F,N] = size(X);
k = numel(K);

Ind = []; nInd = size(k,1);
for i = 1:k
    Ind = union(Ind,K{i}); nInd(i) = numel(K{i});
end
if  numel(Ind) < sum(nInd), fprintf('All classes in K should be disjoint!\n'); return; end
if  sum(nInd) < N, fprintf('Every column in X should belong to one class in K!\n'); return; end

Y = zeros(size(V));
for i = 1:k
    for j = 1:nInd(i)
        %sw = sw + (X(:,K{i}(j))-mu(:,i))*(X(:,K{i}(j))-mu(:,i)).';
        w = X(:,K{i}(j))-mu(:,i);
        Y = Y + w*(w.'*V);
    end
end
end