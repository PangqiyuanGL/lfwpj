% Classical Linear Discriminant Analysis
% Inputs: 
%  X : a sample matrix with F features(rows) and N samples(columns)
%  K : an index set with k classes(cells) and j-th cell contains the indcies of j-th class
%  d : the dimensionality you want to reduce to, should be less than k.
% Outputs:
%  Y : a sample matrix with d features(rows) and N samples(columns)
%
% Copy@right reserved, Qiyuan Pang, April 6, 2020

function [Y,Sb,Sw,W] = LinearDA(X,K,d)
[F,N] = size(X);
k = numel(K);

% check whether classes in K are disjoint 
Ind = []; nInd = size(k,1);
for i = 1:k
    Ind = union(Ind,K{i}); nInd(i) = numel(K{i});
end
if  numel(Ind) < sum(nInd), fprintf('All classes in K should be disjoint!\n'); return; end
if  sum(nInd) < N, fprintf('Every column in X should belong to one class in K!\n'); return; end

mu_each = zeros(F,k);
for i = 1:k
    mu_each(:,i) = mean(X(:,K{i}),2);
end
mu_all = mean(X,2);
Sb = @(V) Scatter_interclass(X,K,mu_each,mu_all,V);
Sw = @(V) Scatter_inclass(X,K,mu_each,V);
SB = Sb(eye(F));
SW = Sw(eye(F));
% maxit = 20;
% Sw_inv_Sb = @(V) Solve(Sb,Sw,V,maxit);
% Sb = zeros(F,F);
% for i = 1:k
%     Sb = Sb + nInd(i)*(mu_each(:,i)-mu_all)*(mu_each(:,i)-mu_all).';
% end
% Sw = zeros(F,F);
% for i = 1:k
%     sw = zeros(F,F);
%     for j = 1:nInd(i)
%         sw = sw + (X(:,K{i}(j))-mu_each(:,i))*(X(:,K{i}(j))-mu_each(:,i)).';
%     end
%     Sw = Sw + sw;
% end

%[W D] = eigs([Sb,F],[Sw,F],d);
%[W D] = eigs(Sb,F,SW,d);
[W D] = eig(SB,SW,'chol');
D = real(diag(D));
[DD I] = sort(D,'descend');
W = W(:,I(1:d));
for j = 1:d
    W(:,j) = W(:,j)/norm(W(:,j));
end
Y = W.'*X;

end