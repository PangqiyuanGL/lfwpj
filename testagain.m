N = 1000;
F = 500;
X = rand([F,N]);
k = 10;
K = cell(1,k);
for i = 1:k
    K{i} = [(i-1)*N/k+1:i*N/k];
end
n = 5;
d = n^2;
[Y Sb Sw W] = LinearDA(X,K,d);