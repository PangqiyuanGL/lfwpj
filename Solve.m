function Y = Solve(Sb,Sw,B,maxit)
B = Sb(B);
Y = pcg(Sw,B,1e-14);

end