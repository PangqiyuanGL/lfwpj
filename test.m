% test LinearDA
% Copy@right reserved, Qiyuan Pang, April 6, 2020

names = dir('lfw/');
len = length(names);

% find folders in LFW with descend numbers of pictures
num = 200;% number of classes used in LDA
num1 = 5;% number of classes for classification
findnames = ones(num,2);
mins = 1;
for i = 3:len
    name = dir(['lfw/',names(i).name,'/']);
    len1 = length(name);
    if  len1 > 1
        if  len1 > findnames(mins,2)
            findnames(mins,1) = i; findnames(mins,2) = len1;
            [val,mins] = min(findnames(:,2)); 
            mins = mins(1);
        end
    end
end
[A I] = sort(findnames(:,2),'descend');
findnames = findnames(I,:);
findnames(:,2) = findnames(:,2) - 2;

Bignames = cell(1,num1);% classes for classification
for i = 1:num1
    Bignames{i} = names(findnames(i,1)).name;
end
morenames = cell(1,num-num1);% classes for supporting LDA
for i = num1+1:num
    morenames{i} = names(findnames(i,1)).name;
end

% collect data from LFW for LDA training
N = sum(findnames(:,2)) % number of samples
p = 5
Samples = cell(1,N); % dataset
K = cell(1,num); % index set
num2 = 0;
for i = 1:num
    for j = 1:findnames(i,2)
        image = imread(['lfw/',names(findnames(i,1)).name,'/',names(findnames(i,1)).name,...,
            '_',num2str(j,'%04d'),'.jpg']);
        Samples{num2+j} = im2double(image);

    end
    K{i} = [num2+1:num2+findnames(i,2)];
    num2 = num2 + findnames(i,2);
end
fprintf('samples data collected!\n')

n = 12
d = n^2; % dimension to reduce to in LDA
[d1,d2,d3] = size(Samples{1})
dd = floor(d1/p);
NewSamples = cell(1,N);
for i = 1:N
    NewSamples{i} = zeros(p*n,p*n,3);
end
for i = 1:p
    for j = 1:p
        X1 = zeros(dd*dd,N);X2 = X1; X3 = X1;
        for k = 1:N
            imd = Samples{k}((i-1)*dd+1:i*dd,(j-1)*dd+1:j*dd,1); X1(:,k) = imd(:);
            imd = Samples{k}((i-1)*dd+1:i*dd,(j-1)*dd+1:j*dd,2); X2(:,k) = imd(:);
            imd = Samples{k}((i-1)*dd+1:i*dd,(j-1)*dd+1:j*dd,3); X3(:,k) = imd(:);
        end
        Y1 = LinearDA(X1,K,d);
        Y2 = LinearDA(X2,K,d);
        Y3 = LinearDA(X3,K,d);
        
        for k = 1:N
            NewSamples{k}((i-1)*n+1:i*n,(j-1)*n+1:j*n,1) = reshape(Y1(:,k),[n,n]);
            NewSamples{k}((i-1)*n+1:i*n,(j-1)*n+1:j*n,2) = reshape(Y2(:,k),[n,n]);
            NewSamples{k}((i-1)*n+1:i*n,(j-1)*n+1:j*n,3) = reshape(Y3(:,k),[n,n]);
        end
        fprintf('finished: %3d/%3d \n',(i-1)*p+j,p*p);
    end
end


imwrite(NewSamples{1},'figure1.jpg')
imwrite(NewSamples{2},'figure2.jpg')
imwrite(NewSamples{3},'figure3.jpg')
imwrite(NewSamples{4},'figure4.jpg')
imwrite(NewSamples{5},'figure5.jpg')
